# Målet her er å hente ut alle unike ord, bare små bokstaver,
# der vi hiver ut alle spesialtegn.

def unique(streng):
    

    # Gå igjennom alle spesialtegn, og erstatte ett og
    # ett av dem som finnes i strengen med en tom streng.
    for kar in '.n:!,;-?':
        streng = streng.replace(kar,"")        
    # Nå skal strengen ha fått fjernet alle spesialtegn.
    print(streng)
    return set(streng.lower().split())

print(unique("Er dette greit, eller er det IKKE greit? Det er greit, tror jeg!"))